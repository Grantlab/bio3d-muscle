muscle <- function(aln, profile=NULL, seqtype="auto",
                   seqgroup = FALSE, refine=FALSE) {

    if(inherits(aln, "fasta")) {
        infile <- tempfile()
        bio3d::write.fasta(aln, file=infile)
    }
    else {
        infile <- aln
    }

    if(!is.null(profile) & !inherits(profile, "fasta"))
        stop("profile must be of class 'fasta'")


    outfile <- tempfile()

    if(!is.null(profile)) {
        in2 <- tempfile()
        write.fasta(profile, file=in2)
        
        args <- c("-profile", "-in1", infile, "-in2", in2, "-out", outfile)
    }
    else {
        args <- c("-in", infile, "-out", outfile)
    }

    if(refine)
        args <- c(args, "-refine")
    if(!is.null(seqtype))
        args <- c(args, "-seqtype", seqtype)
       
    print(args)
    nargs <- length(args)
    stuff <- .C("muscleR", nargs, as.character(args))

    if(inherits(aln, "fasta"))
        unlink(infile)
    if(!is.null(profile))
        unlink(in2)

    gc()
    return(bio3d::read.fasta(outfile))
}
