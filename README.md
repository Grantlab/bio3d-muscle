# README #

### Install ###

```
install.packages(c("bio3d", "devtools"))
library(bio3d)
library(devtools)
load_all("bio3d.muscle")

seqs <- get.seq(c("4q21_A", "5p21_A"))
aln <- muscle(seqs)
```